## Descripton

This is a set of primitive .NET applications.
They all are managing a trivial phonebook using the same library. The applications are:
- Command-line console applicatin
- WPF application
- Web application

## Common library

The library establishes a few possible sources for the data. Their definition is the same in all projects.
- An XML file (`file`)
- An SQL database (`sql`)
- A FireBase database (`fb`)

The library allows listing the records, adding and removing them.

## Console application

This is a command-line interface to the mentioned library.

The source is defined by a `-s` option. Then the command follows - `list`, `add`, or `del`.

## WPF application

This is a native Windows GUI over the phonebook library.

The source is defined by a `-s` option on the command line.

## Web application

This is a .NET Core service and an HTML page providing the Web UI to the ubiquitous phonebook library.

The source is defined by a `SOURCE` environment variable.
